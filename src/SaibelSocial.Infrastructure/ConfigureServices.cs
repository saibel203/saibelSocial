﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SaibelSocial.Core.IOptions;
using SaibelSocial.Core.Models.Identity;
using SaibelSocial.Infrastructure.DatabaseContext;
using SaibelSocial.Infrastructure.ConfigurationHelpers;

namespace SaibelSocial.Infrastructure;

public static class ConfigureServices
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        const string connectionStringName = "DefaultConnectionString";
        string? defaultDbContextConnectionString = configuration.GetConnectionString(connectionStringName);
        JwtOptions jwtOptions = configuration.GetSection(JwtOptions.SectionName).Get<JwtOptions>()!;

        services.AddDbContext<SocialDbContext>(options => options
            .UseSqlServer(defaultDbContextConnectionString));

        services.AddIdentity<ApplicationUser, ApplicationRole>()
            .AddIdentityOptionsConfiguration()
            .AddDefaultTokenProviders() 
            .AddEntityFrameworkStores<SocialDbContext>();

        services.AddAuthentication()
            .AddSchemeConfiguration()
            .AddJwtBearerConfiguration(jwtOptions);

        return services;
    }
}