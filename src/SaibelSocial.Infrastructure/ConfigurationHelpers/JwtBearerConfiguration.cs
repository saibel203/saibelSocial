﻿using System.Text;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SaibelSocial.Core.IOptions;

namespace SaibelSocial.Infrastructure.ConfigurationHelpers;

public static class JwtBearerConfiguration
{
    public static AuthenticationBuilder AddJwtBearerConfiguration(this AuthenticationBuilder builder,
        JwtOptions jwtOptions)
    {
        return builder.AddJwtBearer(options =>
        {
            const string jsonContentType = "application/json";
            const string invalidTokenError = "invalid_token";
            const string invalidTokenErrorDescription
                = "This request requires a valid JWT access token to be provided";

            const string tokenExpiredHeaderName = "x-token-expired";
            const string tokenExpiredOnErrorDescription
                = "The token expired on ";

            byte[] encodingKey = Encoding.UTF8.GetBytes(jwtOptions.Key);
            SymmetricSecurityKey symmetricSecurityKey = new SymmetricSecurityKey(encodingKey);

            options.Audience = jwtOptions.Audience;
            options.SaveToken = true;
            options.RequireHttpsMetadata = false;

            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidAudience = jwtOptions.Audience,
                ValidIssuer = jwtOptions.Issuer,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = symmetricSecurityKey
            };

            options.Events = new JwtBearerEvents
            {
                OnChallenge = context =>
                {
                    context.HandleResponse();
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                    context.Response.ContentType = jsonContentType;
            
                    if (string.IsNullOrEmpty(context.Error))
                        context.Error = invalidTokenError;
                    if (string.IsNullOrEmpty(context.ErrorDescription))
                        context.ErrorDescription = invalidTokenErrorDescription;
            
                    if (context.AuthenticateFailure is not null &&
                        context.AuthenticateFailure.GetType() == typeof(SecurityTokenExpiredException))
                    {
                        SecurityTokenExpiredException? authenticationException =
                            context.AuthenticateFailure as SecurityTokenExpiredException;
            
                        context.Response.Headers.Add(tokenExpiredHeaderName,
                            authenticationException?.Expires.ToString("o"));
                        context.ErrorDescription = tokenExpiredOnErrorDescription +
                                                   authenticationException?.Expires.ToString("o");
                    }
            
                    return context.Response.WriteAsync(JsonConvert.SerializeObject(new
                    {
                        error = context.Error,
                        description = context.ErrorDescription
                    }));
                }
            };
        });
    }
}