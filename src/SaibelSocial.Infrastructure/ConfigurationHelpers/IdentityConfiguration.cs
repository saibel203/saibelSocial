﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace SaibelSocial.Infrastructure.ConfigurationHelpers;

public static class IdentityConfiguration
{
    public static IdentityBuilder AddIdentityOptionsConfiguration(this IdentityBuilder builder)
    {
        builder.Services.Configure<IdentityOptions>(options =>
        {
            options.Password.RequiredLength = 6;
            options.Password.RequireDigit = false;
            options.Password.RequireLowercase = false;
            options.Password.RequireUppercase = false;
            options.Password.RequireNonAlphanumeric = false;

            options.User.RequireUniqueEmail = true;
        });
        
        return builder;
    }
}
