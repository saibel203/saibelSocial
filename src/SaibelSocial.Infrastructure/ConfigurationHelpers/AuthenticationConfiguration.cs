﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;

namespace SaibelSocial.Infrastructure.ConfigurationHelpers;

public static class AuthenticationConfiguration
{
    public static AuthenticationBuilder AddSchemeConfiguration(this AuthenticationBuilder builder)
    {
        builder.Services.Configure<AuthenticationOptions>(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        });
        
        return builder;
    }
}
