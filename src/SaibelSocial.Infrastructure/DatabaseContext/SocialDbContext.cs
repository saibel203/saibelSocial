﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SaibelSocial.Core.Models.Identity;

namespace SaibelSocial.Infrastructure.DatabaseContext;

public class SocialDbContext : IdentityDbContext<ApplicationUser>
{
    public SocialDbContext(DbContextOptions<SocialDbContext> options)
        : base(options)
    {
    }
}