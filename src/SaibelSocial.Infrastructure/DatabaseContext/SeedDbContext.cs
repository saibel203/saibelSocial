﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace SaibelSocial.Infrastructure.DatabaseContext;

public class SeedDbContext
{
    private readonly SocialDbContext _context;
    private readonly ILogger<SeedDbContext> _logger;

    public SeedDbContext(SocialDbContext context, ILogger<SeedDbContext> logger)
    {
        _context = context;
        _logger = logger;
    }

    public async Task InitialiseDatabaseAsync()
    {
        try
        {
            if (_context.Database.IsSqlServer())
                await _context.Database.MigrateAsync();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while trying to initialize the database");
            throw;
        }
    }

    public async Task SeedDataAsync()
    {
        try
        {
            await TrySeedDataAsync();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while trying to seed the database");
            throw;
        }
    }

    public async Task TrySeedDataAsync()
    {
        
    }
}
