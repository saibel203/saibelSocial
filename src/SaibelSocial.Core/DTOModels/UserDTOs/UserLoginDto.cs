﻿namespace SaibelSocial.Core.DTOModels.UserDTOs;

public class UserLoginDto
{
    public string Username { get; set; } = string.Empty;
    public string Password { get; set; } = string.Empty;
}
