﻿namespace SaibelSocial.Core.IOptions;

public class JwtOptions
{
    public const string SectionName = "JwtSettings";
    
    public string Audience { get; set; } = string.Empty;
    public string Issuer { get; set; } = string.Empty;
    public string Key { get; set; } = string.Empty;
}
