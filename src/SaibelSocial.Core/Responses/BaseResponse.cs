﻿namespace SaibelSocial.Core.Responses;

public class BaseResponse
{
    public string Message { get; set; } = string.Empty;
    public bool Succeed { get; set; }
}
