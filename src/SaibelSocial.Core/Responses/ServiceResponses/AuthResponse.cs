﻿namespace SaibelSocial.Core.Responses.ServiceResponses;

public class AuthResponse : BaseResponse
{
    public string Token { get; set; } = string.Empty;
    public DateTime Expires { get; set; }
}
