﻿using SaibelSocial.Core.DTOModels.UserDTOs;
using SaibelSocial.Core.Responses;
using SaibelSocial.Core.Responses.ServiceResponses;

namespace SaibelSocial.Core.ServiceInterfaces;

public interface IAuthService
{
    Task<AuthResponse> GetUserJwtTokenAsync(UserLoginDto? userLoginDto);
    Task<BaseResponse> CreateUserAsync(UserRegisterDto? userRegisterDto);
}