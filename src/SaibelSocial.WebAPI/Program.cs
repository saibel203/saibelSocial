using SaibelSocial.Core;
using SaibelSocial.Infrastructure;
using SaibelSocial.Infrastructure.DatabaseContext;
using SaibelSocial.WebAPI;
using SaibelSocial.WebAPI.Extensions;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

builder.Services.AddCoreServices(builder.Configuration);
builder.Services.AddInfrastructureServices(builder.Configuration);
builder.Services.AddApiServices(builder.Configuration);

WebApplication app = builder.Build();
IWebHostEnvironment environment = app.Environment;
const string corsPolicyName = "client_cors_options";

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();

    using IServiceScope scope = app.Services.CreateScope();

    SeedDbContext init = scope.ServiceProvider.GetRequiredService<SeedDbContext>();
    await init.InitialiseDatabaseAsync();
    await init.SeedDataAsync();
}

app.UseExceptionMiddlewareHandler(environment);

app.UseHttpsRedirection();
app.UseHsts();

app.UseRouting();

app.UseCors(corsPolicyName);

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
