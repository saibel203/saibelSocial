﻿using AutoMapper;
using SaibelSocial.Core.DTOModels.UserDTOs;
using SaibelSocial.Core.Models.Identity;
using SaibelSocial.Core.Responses.ServiceResponses;
using SaibelSocial.WebAPI.Models.RequestModels;
using SaibelSocial.WebAPI.Models.ResponseModels;

namespace SaibelSocial.WebAPI.MapperProfiles;

public class UserProfile : Profile
{
    public UserProfile()
    {
        CreateMap<LoginRequestModel, UserLoginDto>();
        CreateMap<AuthResponse, LoginResponseModel>();
        
        CreateMap<UserRegisterDto, ApplicationUser>();
        CreateMap<RegisterRequestModel, UserRegisterDto>();
    }
}
