using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace SaibelSocial.WebAPI.Controllers;

public class HomeController : ApiController
{
    [Authorize]
    [HttpGet("get")]
    public IActionResult Get()
    {
        return Ok("Hello world!");
    }
}
