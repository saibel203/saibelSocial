﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SaibelSocial.Core.DTOModels.UserDTOs;
using SaibelSocial.Core.Models.Auxiliary;
using SaibelSocial.Core.Responses;
using SaibelSocial.Core.Responses.ServiceResponses;
using SaibelSocial.Core.ServiceInterfaces;
using SaibelSocial.WebAPI.Models.RequestModels;
using SaibelSocial.WebAPI.Models.ResponseModels;

namespace SaibelSocial.WebAPI.Controllers;

public class AuthController : ApiController
{
    private readonly IAuthService _authService;
    private readonly IMapper _mapper;

    public AuthController(IAuthService authService, IMapper mapper)
    {
        _authService = authService;
        _mapper = mapper;
    }


    [HttpPost(nameof(Register))] // /api/auth/register
    public async Task<IActionResult> Register(RegisterRequestModel? registerRequestModel)
    {
        if (registerRequestModel is null)
            return BadRequest();

        UserRegisterDto userRegisterDto = _mapper.Map<UserRegisterDto>(registerRequestModel);
        BaseResponse createUserResult = await _authService.CreateUserAsync(userRegisterDto);
        ApiError error = new ApiError();

        if (!createUserResult.Succeed)
        {
            error.ErrorCode = BadRequest().StatusCode;
            error.ErrorMessage = createUserResult.Message;
            return BadRequest(error);
        }

        return Ok();
    }

    [HttpPost(nameof(Login))] // /api/auth/login
    public async Task<IActionResult> Login(LoginRequestModel? loginRequestModel)
    {
        UserLoginDto userLoginDto = _mapper.Map<UserLoginDto>(loginRequestModel);
        AuthResponse getTokenResult = await _authService.GetUserJwtTokenAsync(userLoginDto);
        ApiError error = new ApiError();

        if (!getTokenResult.Succeed)
        {
            error.ErrorCode = BadRequest().StatusCode;
            error.ErrorMessage = getTokenResult.Message;
            return BadRequest(error);
        }

        LoginResponseModel loginResponse = _mapper.Map<LoginResponseModel>(getTokenResult);

        return Ok(loginResponse);
    }
}