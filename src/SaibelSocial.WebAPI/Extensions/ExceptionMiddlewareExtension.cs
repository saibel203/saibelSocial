﻿using SaibelSocial.WebAPI.Middlewares;

namespace SaibelSocial.WebAPI.Extensions;

public static class ExceptionMiddlewareExtension
{
    public static void UseExceptionMiddlewareHandler(this IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseMiddleware<ExceptionMiddleware>();
    }
}
