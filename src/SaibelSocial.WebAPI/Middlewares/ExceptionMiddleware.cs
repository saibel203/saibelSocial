﻿using System.Net;
using SaibelSocial.Core.Models.Auxiliary;

namespace SaibelSocial.WebAPI.Middlewares;

public class ExceptionMiddleware
{
    private readonly ILogger<ExceptionMiddleware> _logger;
    private readonly IHostEnvironment _hostEnvironment;
    private readonly RequestDelegate _requestDelegate;

    public ExceptionMiddleware(ILogger<ExceptionMiddleware> logger, IHostEnvironment hostEnvironment,
        RequestDelegate requestDelegate)
    {
        _logger = logger;
        _hostEnvironment = hostEnvironment;
        _requestDelegate = requestDelegate;
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await _requestDelegate(context);
        }
        catch (Exception ex)
        {
            string errorMessage;
            HttpStatusCode statusCode;
            Type errorType = ex.GetType();

            if (errorType == typeof(UnauthorizedAccessException))
            {
                statusCode = HttpStatusCode.Unauthorized;
                errorMessage = "You are not authorized";
            }
            else if (errorType == typeof(KeyNotFoundException)
                     || errorType == typeof(FileNotFoundException))
            {
                statusCode = HttpStatusCode.NotFound;
                errorMessage = "An element was not found";
            }
            else if (errorType == typeof(InvalidOperationException)
                     || errorType == typeof(ArgumentException))
            {
                statusCode = HttpStatusCode.BadRequest;
                errorMessage = "An operation or argument passed failed";
            }
            else
            {
                statusCode = HttpStatusCode.InternalServerError;
                errorMessage = "Some unknown error occurred";
            }

            ApiError response = _hostEnvironment.IsDevelopment()
                ? new ApiError((int) statusCode, ex.Message, ex.StackTrace)
                : new ApiError((int) statusCode, errorMessage);
            
            _logger.LogError(ex, "Error: {Message}", ex.Message);
            
            context.Response.StatusCode = (int) statusCode;
            context.Response.ContentType = "application/json";
            await context.Response.WriteAsync(response.ToString());
        }
    }
}