﻿namespace SaibelSocial.WebAPI.Models.ResponseModels;

public class LoginResponseModel
{
    public string Token { get; set; } = string.Empty;
    public string Message { get; set; } = string.Empty;
    public DateTime Expires { get; set; }
}
