﻿using SaibelSocial.Core.ServiceInterfaces;
using SaibelSocial.Infrastructure.DatabaseContext;
using SaibelSocial.Services;
using SaibelSocial.WebAPI.MapperProfiles;

namespace SaibelSocial.WebAPI;

public static class ConfigureServices
{
    public static IServiceCollection AddApiServices(this IServiceCollection services,
            IConfiguration configuration)
    {
        const string corsPolicyName = "client_cors_options";

        services.AddAutoMapper(typeof(UserProfile));
        
        services.AddCors(options =>
        {
            options.AddPolicy(name: corsPolicyName, builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
            });
        });

        services.AddScoped<SeedDbContext>();

        services.AddTransient<IAuthService, AuthService>();
        
        services.AddControllers();
        
        return services;
    }
}