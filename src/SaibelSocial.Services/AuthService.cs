﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SaibelSocial.Core.DTOModels.UserDTOs;
using SaibelSocial.Core.IOptions;
using SaibelSocial.Core.Models.Identity;
using SaibelSocial.Core.Responses;
using SaibelSocial.Core.Responses.ServiceResponses;
using SaibelSocial.Core.ServiceInterfaces;

namespace SaibelSocial.Services;

public class AuthService : IAuthService
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly ILogger<AuthService> _logger;
    private readonly JwtOptions _jwtOptions;
    private readonly IMapper _mapper;

    public AuthService(UserManager<ApplicationUser> userManager, ILogger<AuthService> logger,
        IOptions<JwtOptions> jwtOptions, IMapper mapper)
    {
        _userManager = userManager;
        _logger = logger;
        _mapper = mapper;
        _jwtOptions = jwtOptions.Value;
    }

    public async Task<BaseResponse> CreateUserAsync(UserRegisterDto? userRegisterDto)
    {
        try
        {
            if (userRegisterDto is null)
                return new BaseResponse
                {
                    Message = "An error occurred while retrieving data about a new user",
                    Succeed = false
                };

            ApplicationUser user = _mapper.Map<ApplicationUser>(userRegisterDto);
            IdentityResult userRegisterResult = await _userManager.CreateAsync(user, userRegisterDto.Password);

            if (!userRegisterResult.Succeeded)
                return new BaseResponse
                {
                    Message = "An error occurred while trying to register a new user",
                    Succeed = false
                };
            
            return new BaseResponse
            {
                Message = "User successfully created",
                Succeed = true
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while trying to register a new user");
            return new BaseResponse
            {
                Message = "An error occurred while trying to register a new user",
                Succeed = false
            };
        }
    }
    
    public async Task<AuthResponse> GetUserJwtTokenAsync(UserLoginDto? userLoginDto)
    {
        try
        {
            if (userLoginDto is null)
                return new AuthResponse
                {
                    Message = "An error occurred while retrieving the user",
                    Succeed = false
                };

            ApplicationUser? user = await _userManager.FindByNameAsync(userLoginDto.Username);

            if (user is null)
                return new AuthResponse
                {
                    Message = "No user with this username was found",
                    Succeed = false
                };

            bool passwordCheckValid = await _userManager.CheckPasswordAsync(user, userLoginDto.Password);

            if (!passwordCheckValid)
                return new AuthResponse
                {
                    Message = "For the user with the specified Username, the specified password is incorrect",
                    Succeed = false
                };

            byte[] jwtKey = Encoding.UTF8.GetBytes(_jwtOptions.Key);
            SigningCredentials jwtCredentials = new SigningCredentials(new SymmetricSecurityKey(jwtKey),
                SecurityAlgorithms.HmacSha256Signature);

            List<Claim> userClaims = new List<Claim>
            {
                new(ClaimTypes.Name, user.Id)
            };

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(userClaims),
                Expires = DateTime.Now.AddDays(3),
                SigningCredentials = jwtCredentials
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            string encryptedToken = tokenHandler.WriteToken(token);

            return new AuthResponse
            {
                Message = "Authentication was successful",
                Succeed = true,
                Token = encryptedToken,
                Expires = DateTime.Now.AddDays(3)
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "An error occurred while attempting to authenticate");
            return new AuthResponse
            {
                Message = "An error occurred while attempting to authenticate",
                Succeed = false
            };
        }
    }
}